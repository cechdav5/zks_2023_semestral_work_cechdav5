package planner.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.TextContent;
import planner.pageobjects.ListPlansPage;

public class PlanListError implements Question<String> {
    @Override
    public String answeredBy(Actor actor) {
        return TextContent.of(ListPlansPage.ERROR_WRAPPER).answeredBy(actor);
    }

    public static Question<String> displayedPlanError() {
        return new PlanListError();
    }
}