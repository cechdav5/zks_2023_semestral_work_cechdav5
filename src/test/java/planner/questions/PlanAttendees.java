package planner.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.TextContent;
import planner.pageobjects.ListPlansPage;

public class PlanAttendees implements Question<String> {
    int n;

    protected PlanAttendees(int n) {
        this.n = n;
    }

    @Override
    public String answeredBy(Actor actor) {
        return TextContent.of(ListPlansPage.nthPlanAttendees(n)).answeredBy(actor);
    }

    public static Question<String> displayedPlanAttendees(int n) {
        return new PlanAttendees(n);
    }
}