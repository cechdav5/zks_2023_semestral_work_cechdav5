package planner.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.TextContent;
import planner.pageobjects.ListPlansPage;

public class PlanName implements Question<String> {
    int n;

    protected PlanName(int n) {
        this.n = n;
    }

    @Override
    public String answeredBy(Actor actor) {
        return TextContent.of(ListPlansPage.nthPlanHeader(n)).answeredBy(actor);
    }

    public static Question<String> displayedPlanName(int n) {
        return new PlanName(n);
    }
}
