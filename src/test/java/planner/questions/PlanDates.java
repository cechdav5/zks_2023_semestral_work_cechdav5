package planner.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.TextContent;
import planner.pageobjects.ListPlansPage;

public class PlanDates implements Question<String> {
    int n;

    protected PlanDates(int n) {
        this.n = n;
    }

    @Override
    public String answeredBy(Actor actor) {
        return TextContent.of(ListPlansPage.nthPlanDates(n)).answeredBy(actor);
    }

    public static Question<String> displayedPlanDates(int n) {
        return new PlanDates(n);
    }
}
