package planner.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.TextContent;
import planner.pageobjects.ListPlansPage;

public class PlanDescription implements Question<String> {
    int n;

    protected PlanDescription(int n) {
        this.n = n;
    }

    @Override
    public String answeredBy(Actor actor) {
        return TextContent.of(ListPlansPage.nthPlanDescription(n)).answeredBy(actor);
    }

    public static Question<String> displayedPlanDescription(int n) {
        return new PlanDescription(n);
    }
}
