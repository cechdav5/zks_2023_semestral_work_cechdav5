package planner;

import net.serenitybdd.screenplay.questions.Visibility;
import planner.pageobjects.ListPlansPage;
import planner.questions.PlanListError;
import planner.tasks.*;
import net.serenitybdd.junit5.SerenityJUnit5Extension;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static planner.questions.PlanName.displayedPlanName;
import static planner.questions.PlanDates.displayedPlanDates;
import static planner.questions.PlanAttendees.displayedPlanAttendees;
import static planner.questions.PlanDescription.displayedPlanDescription;
import static net.serenitybdd.screenplay.GivenWhenThen.*;

import static net.serenitybdd.screenplay.GivenWhenThen.givenThat;

@ExtendWith(SerenityJUnit5Extension.class)
public class PlannerTests {
    Actor james = Actor.named("James");

    @Managed
    private WebDriver theBrowser;

    @BeforeEach
    public void setupActor() {
        givenThat(james).can(BrowseTheWeb.with(theBrowser));
    }
    
    @Test
    public void planFieldsTest() {
        givenThat(james).wasAbleTo(Login.login());
        when(james).attemptsTo(ViewPlan.viewNthPlan(1));
        then(james).should(seeThat(displayedPlanName(1), containsString("Best plan ever")));
        then(james).should(seeThat(displayedPlanDates(1), containsString("16:15 13.01.2024 - 22:00 13.01.2024")));
        then(james).should(seeThat(displayedPlanAttendees(1), containsString("Attendees 10")));
        then(james).should(seeThat(displayedPlanDescription(1), containsString("Lorem ipsum dolor sit amet" +
                ", consectetuer adipiscing elit. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam libero" +
                " tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere" +
                " possimus, omnis voluptas assumenda est, omnis dolor repellendus. Et harum quidem rerum facilis est et expedita" +
                " distinctio. Etiam ligula pede, sagittis quis, interdum ultricies, scelerisque eu. Proin in tellus sit amet nibh" +
                " dignissim sagittis. Fusce tellus. Donec vitae arcu. Sed ac dolor sit amet purus malesuada congue. Etiam neque. Cras " +
                "pede libero, dapibus nec, pretium sit amet, tempor quis. Integer lacinia. Maecenas lorem. Fusce suscipit libero eget elit. Vivamus " +
                "porttitor turpis ac leo. Fusce aliquam vestibulum ipsum. Class aptent taciti sociosqu ad litora torquent per " +
                "conubia nostra, per inceptos hymenaeos.")));
    }

    @Test
    public void planUvTest() {
        givenThat(james).wasAbleTo(Login.login());
        when(james).attemptsTo(ViewUv.viewNthPlanUv(1));
        then(james).should(seeThat(Visibility.of(ListPlansPage.UV_CONTAINER), equalTo(true)));
    }

    @Test
    public void planWeatherTest() {
        givenThat(james).wasAbleTo(Login.login());
        when(james).attemptsTo(ViewWeather.viewNthPlanWeather(1));
        then(james).should(seeThat(Visibility.of(ListPlansPage.WEATHER_CONTAINER), equalTo(true)));
    }

    @Test
    public void planMapTest() {
        givenThat(james).wasAbleTo(Login.login());
        when(james).attemptsTo(ViewMap.viewNthPlanMap(1));
        then(james).should(seeThat(Visibility.of(ListPlansPage.MAP_COTAINTER), equalTo(true)));
    }

    @Test
    public void planNoWeatherTest() {
        givenThat(james).wasAbleTo(Login.login());
        when(james).attemptsTo(ViewWeather.viewNthPlanWeather(2));
        then(james).should(seeThat(PlanListError.displayedPlanError(), containsString("Weather data unavailable for that date")));
    }

    @Test
    public void planNoUvTest() {
        givenThat(james).wasAbleTo(Login.login());
        when(james).attemptsTo(ViewUv.viewNthPlanUv(2));
        then(james).should(seeThat(PlanListError.displayedPlanError(), containsString("UV data unavailable for that date")));
    }
}