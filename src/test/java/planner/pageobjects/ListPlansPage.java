package planner.pageobjects;

import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

@DefaultUrl("http://localhost:3030")
public class ListPlansPage extends PageObject {
    public static Target WEATHER_CONTAINER = Target.the("Weather container").locatedBy("#weather-container");
    public static Target UV_CONTAINER = Target.the("UV container").locatedBy("#uv-container");
    public static Target MAP_COTAINTER = Target.the("Map container").locatedBy("#map-container");
    public static Target ERROR_WRAPPER = Target.the("Error wrapper").locatedBy("#error-wrapper");

    public static Target nthPlanHeader(int n) {
        return Target.the(n + " plan header").locatedBy("#root > div > div > div > div:nth-child(2) > div > div:nth-child(" + n + ") > h2");
    }

    public static Target nthPlanDates(int n) {
        return Target.the(n + " plan dates").locatedBy("#root > div > div > div > div:nth-child(2) > div > div:nth-child(" + n + ") > div > div > div > div:nth-child(1)");
    }

    public static Target nthPlanAttendees(int n) {
        return Target.the(n + " plan attendees").locatedBy("#root > div > div > div > div:nth-child(2) > div > div:nth-child(" + n + ") > div > div > div > div:nth-child(2)");
    }

    public static Target nthPlanDescription(int n) {
        return Target.the(n + " plan description").locatedBy("#root > div > div > div > div:nth-child(2) > div > div:nth-child(" + n + ") > div > div > div > div:nth-child(3)");
    }

    public static Target nthPlanWeatherButton(int n) {
        return Target.the(n + " plan weather button").locatedBy("#root > div > div > div > div:nth-child(2) > div > div:nth-child(" + n + ") > div > div > div > div:nth-child(4) > button:nth-child(1)");
    }

    public static Target nthPlanUVButton(int n) {
        return Target.the(n + " plan UV button").locatedBy("#root > div > div > div > div:nth-child(2) > div > div:nth-child(" + n + ") > div > div > div > div:nth-child(4) > button:nth-child(2)");
    }

    public static Target nthPlanLocation(int n) {
        return Target.the(n + " plan location button").locatedBy("#root > div > div > div > div:nth-child(2) > div > div:nth-child(" + n + ") > div > div > div > div:nth-child(4) > button:nth-child(3)");
    }
}
