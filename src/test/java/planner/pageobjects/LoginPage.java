package planner.pageobjects;

import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

@DefaultUrl("http://localhost:3030/login")
public class LoginPage extends PageObject {
    public static Target EMAIL_INPUT = Target.the("Email input").locatedBy("#formEmail");
    public static Target PASSWORD_INPUT = Target.the("Password input").locatedBy("#formPassword");
    public static Target SUBMIT_BUTTON = Target.the("Submit button").locatedBy("#root > div > div > div > button");
}
