package planner.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Step;
import planner.pageobjects.ListPlansPage;

public class ViewWeather implements Task {

    ListPlansPage lpp;

    int n;

    protected ViewWeather(int n) {
        this.n = n;
    }

    @Override
    @Step("{0} view nth")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Open.browserOn(lpp),
                Click.on(ListPlansPage.nthPlanHeader(n)),
                Click.on(ListPlansPage.nthPlanWeatherButton(n))
        );
    }

    public static ViewWeather viewNthPlanWeather(int n) {
        return Instrumented.instanceOf(ViewWeather.class).withProperties(n);
    }
}

