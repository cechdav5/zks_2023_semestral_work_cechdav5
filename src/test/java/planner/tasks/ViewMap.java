package planner.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Step;
import planner.pageobjects.ListPlansPage;

public class ViewMap implements Task {

    ListPlansPage lpp;

    int n;

    protected ViewMap(int n) {
        this.n = n;
    }

    @Override
    @Step("{0} view nth")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Open.browserOn(lpp),
                Click.on(ListPlansPage.nthPlanHeader(n)),
                Click.on(ListPlansPage.nthPlanLocation(n))
        );
    }

    public static ViewMap viewNthPlanMap(int n) {
        return Instrumented.instanceOf(ViewMap.class).withProperties(n);
    }
}
