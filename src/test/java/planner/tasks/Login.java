package planner.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Step;
import planner.pageobjects.LoginPage;

public class Login implements Task {

    LoginPage lp;

    @Override
    @Step("{0} fill create plan form")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Open.browserOn(lp),
                Enter.theValue("test@gmail.com").into(LoginPage.EMAIL_INPUT),
                Enter.theValue("test").into(LoginPage.PASSWORD_INPUT),
                Click.on(LoginPage.SUBMIT_BUTTON)
        );
    }

    public static Login login() {
        return Instrumented.instanceOf(Login.class).withProperties();
    }
}
