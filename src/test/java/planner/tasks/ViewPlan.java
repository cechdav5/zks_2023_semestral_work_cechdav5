package planner.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Step;
import planner.pageobjects.ListPlansPage;
import planner.pageobjects.LoginPage;

public class ViewPlan implements Task {

    ListPlansPage lpp;

    int n;

    protected ViewPlan(int n) {
        this.n = n;
    }

    @Override
    @Step("{0} view nth")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Open.browserOn(lpp),
                Click.on(ListPlansPage.nthPlanHeader(n))
        );
    }

    public static ViewPlan viewNthPlan(int n) {
        return Instrumented.instanceOf(ViewPlan.class).withProperties(n);
    }
}
