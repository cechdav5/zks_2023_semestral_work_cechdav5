# Information

This repository is a part of the semestral project for course ZKS in year 2023. It contains Java code for testing a simple planner web application developped in React. The source code to the tested application with parametrized and process tests written in cypress can be seen [here](https://gitlab.fel.cvut.cz/cechdav5/via-frontend). The source code for the server providing the client with data can be seen [here](https://gitlab.fel.cvut.cz/cechdav5/via-backend).
